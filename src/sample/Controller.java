package sample;

import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

public class Controller {
    @FXML
    private GridPane main;
    @FXML
    private TextArea txtBuffer;
    @FXML
    private TextField txtCommand;

    public void initialize() {
        txtBuffer.addEventHandler(KeyEvent.KEY_PRESSED,eventHandler);
    }

    EventHandler<KeyEvent> eventHandler = event -> {
        System.out.printf("%s%n", event.getCode());
        if(event.isShiftDown() && event.getCode() == KeyCode.SEMICOLON) {
            System.out.printf("Test%n");
            txtCommand.setVisible(true);
            txtCommand.requestFocus();
        }
    };
}
